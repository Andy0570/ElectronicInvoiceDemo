//
//  AppDelegate.h
//  ElectronicInvoiceDemo
//
//  Created by Qilin Hu on 2018/1/18.
//  Copyright © 2018年 Qilin Hu. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 【说明】
 项目名称：电子发票示例 Demo
 起止日期：2018/01/18～
 功能模块：
 */
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

